package org.swat.version.conflict;

import org.swat.core.utils.ClassVersionUtil;

public class VersionMain {
    public static void main(String[] args) {
        ClassVersionUtil util = new ClassVersionUtil("version-conflict/target/lib");
        util.versionMismatch(51);
    }
}
