#!/usr/bin/env bash

FOLDER=`dirname "$0"`

java -cp ${FOLDER}/version-conflict/target/*:${FOLDER}/version-conflict/target/lib/* org.swat.version.conflict.VersionMain
