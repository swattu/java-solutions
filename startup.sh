#!/usr/bin/env bash
FOLDER=`dirname "$0"`

cd /root/code
mvn clean install
./run.sh
