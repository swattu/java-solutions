#!/usr/bin/env bash
FOLDER=`dirname "$0"`
JAVA7=`docker images -a | grep local-java7`
if [[ -z "$JAVA7" ]]
then
  docker build -t local-java7:latest -f $FOLDER/dockerfile-java.txt .
else
  echo "Java 7 Docker is already built"
fi
