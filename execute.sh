#!/usr/bin/env bash
FOLDER=`dirname "$0"`
${FOLDER}/docker/build.sh
docker build -t java-solutions:latest -f $FOLDER/dockerfile.txt .
docker stop java-solutions
docker rm java-solutions
FOLDER=`pwd`
mkdir ${FOLDER}/m2home
docker run --name=java-solutions -v ${FOLDER}/m2home:/root/.m2 java-solutions:latest
